// package models

// import (
// 	_ "github.com/lib/pq"
// 	"gorm.io/gorm"
// )

// var db *gorm.DB

// type DB struct {
// 	*sql.DB
// }

// func Init() *sql.DB {
// 	var err error
// 	connStr := "user=postgres dbname=crudapi password=Aaka$h1234 host=localhost sslmode=disable port=5432"
// 	db, err = sql.Open("postgres", connStr)

// 	if err != nil {
// 		panic(err)
// 	}
// 	//defer db.Close()

// 	err = db.Ping()
// 	if err != nil {
// 		panic(err)
// 	}
// 	return db
// }

package models

import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type User struct {
	ID   uint    `gorm:"primary key:autoIncrement" json:"id"`
	Task *string `json:"Task"`
}

var DB *gorm.DB

func Init() {
	db, err := gorm.Open(postgres.New(postgres.Config{
		DSN: "host=localhost user=postgres dbname=crudapi password=Aaka$h1234 sslmode=disable",
	}))
	if err != nil {
		panic("Error:Failed to connect to database!")
	}

	db.AutoMigrate(&User{})

	DB = db
}
