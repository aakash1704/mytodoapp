import React, { useEffect } from "react";
import "./App.css";
import { Button, Card, Form } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";
import trash from "./trashh.png";
function Todo({ todo, index, markTodo, removeTodo }) {
  return (
    <div className="todo">
      <span>{todo.Task}</span>
      <div>
        <Button variant="outline-danger" onClick={() => removeTodo(index)}>
          <img src={trash} alt="trash" style={{ maxWidth: "50px" }} />
        </Button>
      </div>
    </div>
  );
}

function FormTodo({ addTodo }) {
  const [value, setValue] = React.useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!value) return;
    addTodo(value);
    setValue("");
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Form.Group>
        <Form.Label>
          <b>Add Todo</b>
        </Form.Label>
        <Form.Control
          type="text"
          className="input"
          value={value}
          onChange={(e) => setValue(e.target.value)}
          placeholder="Add new todo"
        />
      </Form.Group>
      <Button variant="primary mb-3" type="submit">
        Submit
      </Button>
    </Form>
  );
}

function App() {
  const [todos, setTodos] = React.useState([
    {
      text: "This is a sampe todo",
      isDone: false,
    },
  ]);
  useEffect(() => {
    axios
      .get("http://localhost:8080/task")
      .then((res) => setTodos(res.data.data))
      .catch((err) => console.log(err));
  }, []);
  const addTodo = async (text) => {
    await axios
      .post("http://localhost:8080/task", {
        task: text,
      })
      .then((res) => setTodos(res.data.data))
      .catch((err) => console.log(err));
  };

  const markTodo = (index) => {
    const newTodos = [...todos];
    newTodos[index].isDone = true;
    setTodos(newTodos);
  };

  const removeTodo = async (id) => {
    await axios
      .delete("http://localhost:8080/task/" + id)
      .catch((err) => console.log(err));

    await axios
      .get("http://localhost:8080/task")
      .then((res) => setTodos(res.data.data))
      .catch((err) => console.log(err));
  };

  return (
    <div className="app">
      <div className="container">
        <h1 className="text-center mb-4">Todo List</h1>
        <FormTodo addTodo={addTodo} />
        <div>
          {todos.map((todo, index) => (
            <Card>
              <Card.Body>
                <Todo
                  key={index}
                  index={index}
                  todo={todo}
                  markTodo={markTodo}
                  removeTodo={() => removeTodo(todo.id)}
                />
              </Card.Body>
            </Card>
          ))}
        </div>
      </div>
    </div>
  );
}

export default App;
